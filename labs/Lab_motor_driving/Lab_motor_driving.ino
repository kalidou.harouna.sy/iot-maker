
/*Home
 
Tutorials
 
Projects
28BYJ-48 Stepper Motor with ULN2003 Driver and Arduino Tutorial
Written by Benne de Bakker 25 Comments

28byj-48 stepper motor with uln2003 driver and arduino tutorial 
This article includes everything you need to know about controlling a 28BYJ-48 stepper motor with the ULN2003 driver board and Arduino. I have included datasheets, a wiring diagram, and many example codes!

First we take a look at the easy to use Arduino Stepper library. This library is great when you are just starting out, but doesn’t have many extra features.

I highly recommend to also take a look at the example codes for the AccelStepper library at the end of this tutorial. This library is fairly easy to use and can greatly improve the performance of your hardware.

After each example, I break down and explain how the code works, so you should have no problems modifying it to suit your needs.

If you want to learn more about controlling larger stepper motors with more torque and more speed, take a look at the articles below. In these articles I teach you how to control NEMA 17 stepper motors, with drivers like the A4988.

Other stepper motor tutorials:
Control a stepper motor with L298N motor driver and Arduino
How to control a Stepper Motor with Arduino Motor Shield Rev3
How to control a stepper motor with A4988 driver and Arduino
How to control a stepper motor with DRV8825 driver and Arduino
If you have any questions, please leave a comment below.

Supplies
Hardware components
28byj-48-stepper-motor  28BYJ-48 stepper motor  × 1 Amazon
ULN2003 driver board  × 1 Amazon
Arduino Uno Rev 3 Arduino Uno Rev3  × 1 Amazon
Jumper wires (male to female) × 10  Amazon
AliExpress
Breadboard (optional, makes wiring easier)  × 1 Amazon
USB cable type A/B  × 1 Amazon
5V power supply (powering the stepper motor directly from the Arduino can damage it!) × 1 Amazon
Software
Arduino IDE Arduino IDE   
Makerguides.com is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking to products on Amazon.com.

Information about the 28BYJ-48 stepper motor and ULN2003 driver board
The 28BYJ-48 is one of the cheapest stepper motors you can find. Although it is not super accurate or powerful, it is a great motor to use for smaller projects or if you just want to learn about stepper motors.

This motor is often used to automatically adjust the vanes of an air conditioner unit. It has a built-in gearbox, which gives it some extra torque and reduces the speed drastically.

Below you can find the specifications for both the stepper motor and driver that are used in this tutorial.

28BYJ-48 Stepper Motor Specifications
Rated voltage 5 V
Coil Resistance 50 Ohms
Coil Type Unipolar
Diameter – shaft  0.197″ (5.00 mm)
Length – shaft and bearing  0.394″ (10 mm)
Features  Flatted shaft
Size/dimension  Round – 1.100″ dia (28.00 mm)
Mounting hole spacing Flatted Shaft
Gear reduction  1/64 (see note)
Step angle  Half step mode (recommended): 0.0879°
Full step mode: 0.176°
Steps per revolution  Half step mode: 4096 (see note)
Full step mode: 2048
Termination style Wire leads with connector
Motor type  Permanent Magnet Gear Motor
Number of phases  4
Cost  Check price
For more information you can check out the datasheet here.

28BYJ-48 Datasheet
Important note: Manufacturers usually specify that the motors have a 64:1 gear reduction. Some members of the Arduino Forums noticed that this wasn’t correct and so they took some motors apart to check the actual gear ratio. They determined that the exact gear ratio is in fact 63.68395:1, which results in approximately 4076 steps per full revolution (in half step mode).


I am not sure if all manufacturers use the exact same gearbox, but you can just adjust the steps per revolution in the code, to match your model.

The Adafruit Industries Small Reduction Stepper Motor uses the same form factor as the 28BYJ-48, but does have a different gear ratio. It has a roughly 1/16 reduction gear set, which results in 513 steps per revolution (in full-step mode). You can download the datasheet for it here.

For more information about the driver you can check out the datasheet below.

ULN2003 Datasheet
ULN2003 Driver PCB
Wiring – Connecting 28BYJ-48 stepper motor and ULN2003 driver board to Arduino UNO
The wiring diagram/schematic below shows you how to connect the ULN2003 driver board to the 28BYJ-48 stepper motor and the Arduino. The connections are also given in the table below.

28BYJ-48-Stepper-Motor-ULN2003-Driver-Wiring-Diagram-Schematic-Pinout 
Wiring diagram for ULN2003 driver with 28BYJ-48 stepper motor and Arduino.
I used a breadboard and some jumper wires to connect the driver board to an external power supply.

ULN2003 and 28BYJ-48 to Arduino Connections
ULN2003 Driver Board  Connection
IN1 Pin 8 Arduino
IN2 Pin 9 Arduino
IN3 Pin 10 Arduino
IN4 Pin 11 Arduino
– Logic GND Arduino
– GND power supply
+ 5 V power supply
Please note: It is possible to directly power the stepper motor from the 5 V output of the Arduino. This however, is not recommended. When the stepper motor draws too much current you can damage the Arduino. I also found that when powering the Arduino with USB power only, I would get inconsistent behavior and bad performance of the stepper motor.

I recommend to power the driver board/stepper motor with an external 5 V power supply, like this one. It should come with a female DC connector, so you can easily connect it to some (jumper) wires. Note that you also need to connect the GND of the Arduino to the – pin on the ULN2003 driver board.

After uploading the code you also need to power the Arduino, either with a USB type-B cable or via the 5.5 mm power jack.

The jumper next to power connections on the driver board can be used to disconnect power to the stepper motor.


Basic Arduino example code to control a 28BYJ-48 stepper motor
You can upload the following example code to your Arduino using the Arduino IDE.

This example uses the Stepper.h library, which should come pre-installed with the Arduino IDE. This sketch turns the stepper motor 1 revolution in one direction, pauses, and then turns 1 revolution in the other direction.

/* Example sketch to control a 28BYJ-48 stepper motor with ULN2003 driver board and Arduino UNO. More info: https://www.makerguides.com */
// Include the Arduino Stepper.h library:
#include <Stepper.h>
// Define number of steps per rotation:
const int stepsPerRevolution = 2048;
// Wiring:
// Pin 8 to IN1 on the ULN2003 driver
// Pin 9 to IN2 on the ULN2003 driver
// Pin 10 to IN3 on the ULN2003 driver
// Pin 11 to IN4 on the ULN2003 driver
// Create stepper object called 'myStepper', note the pin order:
Stepper myStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);
void setup() {
  // Set the speed to 5 rpm:
  myStepper.setSpeed(5);
  
  // Begin Serial communication at a baud rate of 9600:
  Serial.begin(9600);
}
void loop() {
  // Step one revolution in one direction:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(500);
  
  // Step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution);
  delay(500);
}
